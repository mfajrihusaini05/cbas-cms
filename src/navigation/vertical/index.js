/*

Array of object

Top level object can be:
1. Header
2. Group (Group can have navItems as children)
3. navItem

* Supported Options

/--- Header ---/

header

/--- nav Grp ---/

title
icon (if it's on top level)
tag
tagVariant
children

/--- nav Item ---/

icon (if it's on top level)
title
route: [route_obj/route_name] (I have to resolve name somehow from the route obj)
tag
tagVariant

*/

// Array of sections
export default [
  {
    title: 'Dashboards',
    icon: 'HomeIcon',
    route: 'dashboards',
  },
  {
    title: 'Master Data',
    icon: 'DatabaseIcon',
    children: [
      {
        title: 'Mapping FC',
        children: [
          {
            title: 'Approved',
            route: 'mapping-fc',
          },
          {
            title: 'Requested',
            route: 'mapping-fc-requested',
          },
        ],
      },
      {
        title: 'Action',
        children: [
          {
            title: 'Approved',
            route: 'action-lists',
          },
          {
            title: 'Requested',
            route: 'action-requested',
          },
        ],
      },
      {
        title: 'Mapping - DC',
        children: [
          {
            title: 'Approved',
            route: 'mapping-dc-lists',
          },
          {
            title: 'Requested',
            route: 'mapping-dc-requested',
          },
        ],
      },
      {
        title: 'Account Distribution',
        children: [
          {
            title: 'Approved',
            route: 'account-distribution-lists',
          },
          {
            title: 'Requested',
            route: 'account-distribution-requested',
          },
        ],
      },
      {
        title: 'Branch',
        children: [
          {
            title: 'Approved',
            route: 'branch-lists',
          },
          {
            title: 'Requested',
            route: 'branch-requested',
          },
        ],
      },
    ],
  },
  {
    title: 'Log Out',
    icon: 'PowerIcon',
    route: 'logout',
  },
]
