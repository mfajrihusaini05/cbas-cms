export default [
  {
    path: '/transaksi/submitcustomer',
    name: 'transaksi/submitcustomer',
    component: () => import('@/views/master/transaksi/TransaksiSubmit.vue'),
    meta: {
      pageTitle: () => 'Data To Be Process',
      breadcrumb: [
        {
          text: 'Data To Be Process',
          active: true,
        },
      ],
      authRequired: true,
    },
  },
  {
    path: '/transaksi/form',
    name: 'transaksi/form',
    component: () => import('@/views/master/transaksi/Add.vue'),
    meta: {
      pageTitle: () => 'Add Form',
      breadcrumb: [
        {
          text: 'Data To Be Process',
          to: '/transaksi/submitcustomer',
        },
        {
          text: 'Add Form',
          active: true,
        },
      ],
      authRequired: true,
    },
  },
]
