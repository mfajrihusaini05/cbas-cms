export default [
  // Link User
  {
    path: '/user/list',
    name: 'user/list',
    component: () => import('@/views/master/users/ListUser.vue'),
    meta: {
      pageTitle: () => 'User',
      breadcrumb: [
        {
          text: 'User Lists',
          active: true,
        },
      ],
      authRequired: true,
    },
  },
  {
    path: '/user/form',
    name: 'user/form',
    component: () => import('@/views/master/users/FormCreateEdit.vue'),
    meta: {
      pageTitle: () => 'Add User',
      breadcrumb: [
        {
          text: 'User Lists',
          to: '/user/list',
        },
        {
          text: 'Add User',
          active: true,
        },
      ],
      authRequired: true,
    },
  },
  {
    path: '/user/form/:id',
    name: 'user/form/:id',
    component: () => import('@/views/master/users/FormCreateEdit.vue'),
    meta: {
      pageTitle: () => 'Edit User',
      breadcrumb: [
        {
          text: 'User Lists',
          to: '/user/list',
        },
        {
          text: 'Edit User',
          active: true,
        },
      ],
      authRequired: true,
    },
  },

  // Route Cabang
  {
    path: '/cabang',
    name: 'cabang',
    component: () => import('@/views/master/branch/Lists.vue'),
    meta: {
      pageTitle: () => 'Cabang List',
      breadcrumb: [
        {
          text: 'Cabang List',
          active: true,
        },
      ],
      authRequired: true,
    },
  },
  {
    path: '/cabang/form',
    name: 'cabang/form',
    component: () => import('@/views/master/branch/FormCreateEdit.vue'),
    meta: {
      pageTitle: () => 'Add Cabang',
      breadcrumb: [
        {
          text: 'Cabang List',
          to: '/cabang',
        },
        {
          text: 'Add Cabang',
          active: true,
        },
      ],
      authRequired: true,
    },
  },
  {
    path: '/cabang/form/:id',
    name: 'cabang/form/:id',
    component: () => import('@/views/master/branch/FormCreateEdit.vue'),
    meta: {
      pageTitle: () => 'Edit Cabang',
      breadcrumb: [
        {
          text: 'Cabang List',
          to: '/cabang',
        },
        {
          text: 'Edit Cabang',
          active: true,
        },
      ],
      authRequired: true,
    },
  },
]
